Feature: Smoke test splitwise
	I want to login to splitwise website for adding expenses between friends with the details provided
Scenario: Login to splitwise and add a bill
Given Open the GoogleChrome browser
	And Open the webapp
When Login to splitwise with valid username and password
	And Click on Add a Bill
	And Add the details for the bill and save
	Then I should be able to validate that the bill is added 
	And Browser should be closed