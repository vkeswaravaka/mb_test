package stepDefinition;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

public class SmokeTest {

	WebDriver driver;

	@Given("^Open the GoogleChrome browser$")
	public void open_the_GoogleChrome_browser() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "driver/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
	}

	@Given("^Open the webapp$")
	public void open_the_webapp() throws Throwable {
		driver.get("http://www.splitwise.com");
	}

	@When("^Login to splitwise with valid username and password$")
	public void login_to_splitwise_with_valid_username_and_password() throws Throwable {
		driver.findElement(By.linkText("Log in")).click();
		driver.findElement(By.id("user_session_email")).sendKeys("sample123@gmail.com");
		driver.findElement(By.id("user_session_password")).sendKeys("sample123");
		driver.findElement(By.name("commit")).click();
	}

	@When("^Click on Add a Bill$")
	public void click_on_Add_a_Bill() throws Throwable {
		driver.findElement(By.linkText("Add a bill")).click();

	}

	@When("^Add the details for the bill and save$")
	public void add_the_details_for_the_bill_and_save() throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement addBillModal = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.id("token-input-add_bill_with")));
		addBillModal.sendKeys("sample@gmail.com");
		addBillModal.sendKeys(Keys.RETURN);
		addBillModal.findElement(By.xpath("//*[@id='add_bill']/div/div[2]/div[2]/div[1]/input"))
				.sendKeys("Sample Bill");
		addBillModal.findElement(By.xpath("//*[@id='add_bill']/div/div[2]/div[2]/div[1]/div[2]/input")).sendKeys("20");
		addBillModal.findElement(By.xpath("//*[@id='add_bill']/div/div[2]/div[2]/div[1]/div[2]/span")).click();
		WebElement currencyModal = wait.until(ExpectedConditions
				.elementToBeClickable(driver.findElement(By.xpath("//*[@id='choose_currency']/div/ul/li[41]"))));
		currencyModal.click();
		addBillModal.findElement(By.xpath("//*[@id='add_bill']/div/div[2]/div[2]/a[1]")).click();
		WebElement dateModal = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//*[@id='datepicker']/div/div[1]/table/tbody/tr[5]/td[7]")));
		Thread.sleep(200);
		dateModal.findElement(By.xpath("//*[@id='datepicker']/div/div[1]/table/tbody/tr[5]/td[7]")).click();
		addBillModal.findElement(By.xpath("//*[@id='add_bill']/div/div[2]/footer/button[2]")).click();
	}

	@Then("^I should be able to validate that the bill is added$")
	public void i_should_be_able_to_validate_that_the_bill_is_added() throws Throwable {
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='people_summary']/div[2]/div[2]/ul/li/a")).click();
		Thread.sleep(2000);
		List<WebElement> expenses = driver.findElements(By.className("expense"));
		WebElement item = expenses.get(0);
		String label = item.getText();
		Assert.assertTrue("Bill not added", expenses.size() > 0);
		Assert.assertEquals("Bill not added", "OCT\n28\nSample Bill\nyou paid\nEUR20.00 you lent Sampel S.\nEUR10.00",
				label);
	}

	@Then("^Browser should be closed$")
	public void browser_should_be_closed() throws Throwable {
		driver.close();
	}
}
